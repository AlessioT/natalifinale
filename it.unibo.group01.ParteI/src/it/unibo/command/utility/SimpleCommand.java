package it.unibo.command.utility;

public class SimpleCommand implements ICommand {
	private CommandType type;
	private String timeout;
	
	public SimpleCommand(CommandType tipo, String timeout){
		this.type=tipo;
		this.timeout=timeout;
	}
	
	@Override
	public String getName() {
		return type.toString();
	}

	@Override
	public String getTimeout() {
		return timeout;
	}
	
	@Override
	public String toString(){
		return getName()+getTimeout();
	}

}
