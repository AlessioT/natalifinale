package it.unibo.command.utility;

public interface ICommand {
	String getName();
	String getTimeout();
	String toString();
}
