package it.unibo.command.utility;

public class SimplePosition implements IPosition {
	
	private int x;
	private int y;
	private boolean onX;
	
	public SimplePosition(int x, int y, boolean onX){
		this.x=x;
		this.y=y;
		this.onX=onX;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public boolean isOnX() {
		return onX;
	}

}
