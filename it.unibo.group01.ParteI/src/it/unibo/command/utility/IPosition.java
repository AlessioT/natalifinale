package it.unibo.command.utility;

public interface IPosition {
	int getX();
	int getY();
	boolean isOnX();
}
