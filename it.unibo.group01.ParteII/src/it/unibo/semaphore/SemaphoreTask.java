/*
*  Generated by AN Unibo 
*/
package it.unibo.semaphore ;

import java.util.ArrayList;

public class SemaphoreTask extends  SemaphoreTaskSupport {
	public  SemaphoreTask(String name, boolean withGui) throws Exception {
		super(name, withGui);
	}

	@Override
	protected void initialize() throws Exception {
		robotQueue=new ArrayList<String>();
	}

	@Override
	protected String dequeue() throws Exception {
		String result=robotQueue.get(0);
		robotQueue.remove(0);
		return result;
	}
}
