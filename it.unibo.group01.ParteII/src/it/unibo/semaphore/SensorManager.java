 package it.unibo.semaphore;

import it.unibo.event.interfaces.INodejsLike;
import it.unibo.iot.configuration.IConfiguration;
import it.unibo.iot.configurator.Configurator;
import it.unibo.iot.models.sensorData.IDetection;
import it.unibo.iot.sensors.detector.IDetectorObservable;
import it.unibo.iot.sensors.detector.IDetectorObserver;

public class SensorManager extends SensorManagerSupport {

	private String robotName;
	
	public SensorManager(String name, INodejsLike njs, boolean withGui)	throws Exception {
		super(name, njs, withGui);
		// TODO Auto-generated constructor stub
	}

	public void configure(String name){
		robotName=name;
	}
	
	@Override
	protected void doJob() throws Exception {
		System.out.println("Sensor Started");
		IConfiguration conf = Configurator.getConfiguration();

		IDetectorObserver obsDetectorObserver = new IDetectorObserver() {

			@Override
			public void notify(IDetection detection) {
				try {
					if(detection.getVal())
						System.out.println("line detected "+detection.getDirection()+" "+detection.getVal());
						raiseEventlineDetected(robotName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		IDetectorObservable [] detectorObservables = conf.getLineDetectorObservables();
		detectorObservables[0].addObserver(obsDetectorObserver);
		
		/*for (IDetectorObservable iDetectorObservable : detectorObservables) {
			iDetectorObservable.addObserver(obsDetectorObserver);
		}*/
		
	}
	

}
