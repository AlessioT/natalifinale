package it.unibo.semaphore;

import it.unibo.event.interfaces.INodejsLike;

public class StoppableTimer extends StoppableTimerSupport{

	private long sleepTime;
	private String robotName;
	
	public StoppableTimer(String name, INodejsLike njs, boolean withGui)
			throws Exception {
		super(name, njs, withGui);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doJob() throws Exception {
		//System.out.println("Timer ready, sleeping for "+sleepTime+" robotName:"+robotName);
		Thread.sleep(sleepTime);
		//System.out.println("raising time elapsed");
		this.raiseEventtimeElapsed(robotName);
	}
	
	public void configure(String millisec,String robotName){
		sleepTime=Long.parseLong(millisec);
		this.robotName=robotName;
	}

}
