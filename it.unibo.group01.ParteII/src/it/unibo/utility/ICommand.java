package it.unibo.utility;

public interface ICommand {
	String getName();
	String getTimeout();
	String toString();
}
