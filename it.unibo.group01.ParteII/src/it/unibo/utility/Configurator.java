package it.unibo.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Configurator {

	public static String FILENAME="robotName.txt";
	
	public static String getRobotName() throws IOException{
		BufferedReader read=new BufferedReader(new FileReader(new File(FILENAME)));
		String name=read.readLine().trim();
		//System.out.println("Ciao il mio nome è: "+name);
		return name;
	}
}
