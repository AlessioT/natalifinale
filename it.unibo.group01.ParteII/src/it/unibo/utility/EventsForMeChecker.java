package it.unibo.utility;

public class EventsForMeChecker {
	
	public static boolean isEventForMe(String msgID,String msg,String name){
		switch(msgID){
			case "start":
				return true;
			case "superstop":
				return true;
			case "cmd":
				return msg.split(" ")[1].equals(name);
			case "timeElapsed":
				return msg.equals(name);
			case "setTimer":
				return msg.split(" ")[1].equals(name);
			case "transitRequest":
				return msg.equals(name);
			case "transitApproval":
				return msg.equals(name);
			case "transitComplete":
				return msg.equals(name);
			case "lineDetected":
				return msg.equals(name);
			default:
				return false;
		}
	}
}
