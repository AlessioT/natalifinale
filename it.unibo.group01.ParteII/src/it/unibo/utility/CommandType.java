package it.unibo.utility;

public enum CommandType {
	FORWARD,
	FORWARDLEFT,
	FORWARDRIGHT,
	BACKWARD,
	BACKWARDLEFT,
	BACKWARDRIGHT,
	LEFT,
	RIGHT,
	STOP;
}
