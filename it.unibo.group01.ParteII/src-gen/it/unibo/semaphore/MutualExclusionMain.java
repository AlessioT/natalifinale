/*
*  Generated by AN Unibo 
*/
package it.unibo.semaphore;
import it.unibo.event.interfaces.INodejsLike;
import it.unibo.nodelike.platform.NodejsLike;
import it.unibo.inforeply.infrastructure.SysKbInfoReplInfr;
import java.io.FileInputStream;
import it.unibo.inforeply.infrastructure.PerceiveConnectionThread;
public abstract class MutualExclusionMain{
 
protected INodejsLike njs;
protected SysKbInfoReplInfr sysInfr = null;
	protected RobotBehaviour robotBehaviour;
	protected RobotCommander robotCommander;
	protected MutualExclusionCommander mutualExclusionCommander;
	protected Mytimer mytimer;
	protected SemaphoreTask semaphoreTask;
	protected PrenotationSimulator prenotationSimulator;
 	public void doJob(){
 		try{
 	      initProperty();
 	      init();
 	      configure(); 
 	      start();
 	   }catch(Exception e){
 			System.out.println(" !!! ERROR " + e.getMessage() );    
 			e.printStackTrace();	
 	   }
 	}
	protected void initProperty() throws Exception{
	}
	protected void init() throws Exception{
	createEventDitribuedInfrastructure("mutualExclusion", ApplKb.outEvents,ApplKb.inEvents);
	}
	protected void createEventDitribuedInfrastructure(String name,
	String[] myEvents, String[] otherEvents) throws Exception {
	 try {
	     sysInfr = SysKbInfoReplInfr.getInstance(null,
	       	new FileInputStream("semaphoreconfig.pl") );
	      sysInfr.createInfoReplInfrastructure(name, myEvents);
	      initSupport();
	      //sysInfr.waitForAllConnection();
	  } catch (Exception e) {
	  	 System.out.println("createEventInfrastructure ERROR " + e.getMessage());
	 }
	}
	protected void initSupport() throws Exception{
		njs = NodejsLike.createBasic(null); 
		 	njs.setWithExternalEvents();
	}
	protected void configure() throws Exception{
		mutualExclusionCommander = MutualExclusionCommanderSupport.create("mutualExclusionCommander", false);  
	}
	protected void start() throws Exception{
		njs.insertInRunQueue(mutualExclusionCommander);
		njs.startMainLoop();
	}
 	public static void main(String args[]) {
 	  MutualExclusion system = new MutualExclusion( );
 	  system.doJob();
 	}
}//MutualExclusionMain
