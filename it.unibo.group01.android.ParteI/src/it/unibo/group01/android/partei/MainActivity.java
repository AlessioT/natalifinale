package it.unibo.group01.android.partei;

import it.unibo.event.interfaces.INodejsLike;
import it.unibo.group01.partei.StopMachineMain;
import it.unibo.group01.partei.SuperStopSimulator;
import it.unibo.group01.partei.SuperStopSimulatorSupport;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {

	protected INodejsLike njs=StopMachineMain.getInstanceOfNJS();
	protected SuperStopSimulator sss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    
    	
    	try {sss=SuperStopSimulatorSupport.create("SuperStopSimulator", false);} 
    	catch (Exception e) {e.printStackTrace();}
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        */
        
        ((Button)findViewById(R.id.buttonStop)).setOnClickListener(
        		new OnClickListener() {
					@Override
					public void onClick(View v) {
						try{MainActivity.this.njs.raiseEvent(MainActivity.this.sss,"superstop", "Stop");}
						catch(Exception e){e.printStackTrace();}
					}
				});
        ((Button)findViewById(R.id.buttonStart)).setOnClickListener(
        		new OnClickListener() {
					@Override
					public void onClick(View v) {
						try{
							INodejsLike njs = MainActivity.this.njs;
							SuperStopSimulator sss = MainActivity.this.sss;
							njs.raiseEvent(sss,"start", "Start");
							}
						catch(Exception e){e.printStackTrace();}
					}
				});
        ((Button)findViewById(R.id.buttonEnter)).setOnClickListener(
        		new OnClickListener() {
					@Override
					public void onClick(View v) {
						try{
							INodejsLike njs = MainActivity.this.njs;
							SuperStopSimulator sss = MainActivity.this.sss;
							njs.raiseEvent(sss,"transitRequest", "SimulatedRequest");
							((Button)findViewById(R.id.buttonEnter)).setEnabled(false);
							((Button)findViewById(R.id.buttonExit)).setEnabled(true);
							}
						catch(Exception e){e.printStackTrace();}
					}
				});
        ((Button)findViewById(R.id.buttonExit)).setOnClickListener(
        		new OnClickListener() {
					@Override
					public void onClick(View v) {
						try{
							INodejsLike njs = MainActivity.this.njs;
							SuperStopSimulator sss = MainActivity.this.sss;
							njs.raiseEvent(sss,"transitComplete", "SimulatedRequest");
							((Button)findViewById(R.id.buttonEnter)).setEnabled(true);
							((Button)findViewById(R.id.buttonExit)).setEnabled(false);
							}
						catch(Exception e){e.printStackTrace();}
					}
				});
        ((Button)findViewById(R.id.buttonExit)).setEnabled(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }*/

}
