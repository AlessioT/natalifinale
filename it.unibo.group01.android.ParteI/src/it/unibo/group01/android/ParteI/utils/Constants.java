/**Hand-Generated by us**/
package it.unibo.group01.android.ParteI.utils;

public class Constants {
	/**
	%semaphoreconfig.pl
	startUpMode(synch).
	node("robot" , 1 , "192.168.1.131", 12345).
	node("timerMachine" , 2 , "192.168.1.131", 11111).
	node("stopMachine" , 3 , "192.168.1.130", 54321).
	node("semaphore" , 4 , "192.168.1.130", 22222).
	 **/
	public static final String IP_ROBOT			=	"192.168.1.129";
	public static final int PORT_ROBOT			=	12345;
	public static final String IP_TIMERMACHINE	=	"192.168.1.129";
	public static final int PORT_TIMERMACHINE	=	11111;
	public static final String IP_STOPMACHINE	=	"192.168.1.131";
	public static final int PORT_STOPMACHINE	=	54321;
	public static final String IP_SEMAPHORE		=	"192.168.1.130";
	public static final int PORT_SEMAPHORE		=	22222;
	public static final String IP_MUTEX			=	"192.168.1.129";
	public static final int PORT_MUTEX			=	33333;
	
	public static final String PrologNetworkRepresentation()
	{
		StringBuilder content = new StringBuilder();
		content.append("startUpMode(synch).\n")
		 	.append("node(\"robot\" , 1 , \""+IP_ROBOT+"\", "+PORT_ROBOT+").\n")
		 	.append("node(\"timerMachine\" , 2 , \""+IP_TIMERMACHINE+"\", "+PORT_TIMERMACHINE+").\n")
		 	.append("node(\"stopMachine\" , 3 , \""+IP_STOPMACHINE+"\", "+PORT_STOPMACHINE+").\n")
			.append("node(\"semaphore\" , 4 , \""+IP_SEMAPHORE+"\", "+PORT_SEMAPHORE+").\n")
			.append("node(\"mutualExclusion\" , 5 , \""+IP_MUTEX+"\", "+PORT_MUTEX+").\n");
		return content.toString();
	}
	

	
}
