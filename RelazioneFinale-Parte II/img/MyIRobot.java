package it.unibo.iot.robot;

import it.unibo.iot.models.robotCommands.IRobotCommand;

public interface MyIRobot {

	public void execute(IRobotCommand command);

	public IRobotPosition getPosition();

	public void transitRequest();

	public void setSemaphore(ISemaphore semaphore);

}
