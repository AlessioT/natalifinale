import java.util.List;


public interface ISemaphore {
	
	void transit(String robotName);
	void transitComplete(String robotName);
	boolean isFree();
	List<String> getQueue();
	
}
