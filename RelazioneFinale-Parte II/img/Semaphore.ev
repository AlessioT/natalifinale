EventSystem semaphore

Context robotContext ip "192.168.1.129" port 12345;
Context timerMachine ip "192.168.1.129" port 11111;
Context stopMachine ip "192.168.1.130" port 54321;
Context semaphore ip "192.168.1.130" port 22222;

Event start;
Event cmd;
Event superstop;
Event timeElapsed;
Event setTimer;
Event transitRequest;
Event transitApproval;
Event transitComplete;
Event lineDetected;

Task robot context robotContext listenTo superstop cmd;
Task robotCommander context robotContext listenTo start timeElapsed transitRequest transitApproval;
Task mutualExclusionCommander context robotContext listenTo lineDetected transitApproval;
Task mytimer context timerMachine listenTo setTimer transitApproval transitRequest; 
Task semaphoreTask context semaphore listenTo transitRequest transitComplete;

Task prenotationSimulator context stopMachine listenTo transitApproval;

External raiseStop context stopMachine raising superstop start;
External sensorManager context robotContext raising lineDetected;
External stoppableTimer context timerMachine raising timeElapsed;

BehaviorOf robot
{
	var it.unibo.event.interfaces.IEventItem curEventItem = null
	var String curevId = ""
	var String robotName = ""
	
	action void executeCmd(String cmdToExecute)
	
	state init initial 
		showMsg("starting...")
		set robotName = call it.unibo.utility.Configurator.getRobotName()
		goToState waitCmd
	endstate
	
	state waitCmd
		onEvents cmd or superstop goToState executeCmd
	endstate 
	
	state executeCmd
		acquireEvent curEventItem
		set curevId = call curEventItem.getEventId()
		if(%curEventItem!=null){
			if(%curevId.equals("superstop")){
				goToState superStop
			}
		}
		if(call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)){
			call executeCmd(call curEventItem.getMsg())
		}
		goToState waitCmd
	endstate
	
	state superStop
		showMsg("stopping..")
		call executeCmd(call it.unibo.utility.MessageBuilder.buildMsg("STOP",%robotName))
		transitToEnd
	endstate
		
}

BehaviorOf robotCommander{
	var it.unibo.event.interfaces.IEventItem curEventItem = null
	var String curevId = ""
	var it.unibo.utility.ICommand nextCmd = null
	var boolean isLast = false
	var String robotName = ""
	var boolean eventsForMe = false
	
	action it.unibo.utility.ICommand getNextCmd()
	action void readConfiguration()
	
	state init initial
		showMsg("robotCommander init")
		set robotName = call it.unibo.utility.Configurator.getRobotName()
		onEvent start goToState configure
	endstate
	
	state configure  
		showMsg("reading configuration file")
		call readConfiguration()
		goToState sendCmd
	endstate
	
	state sendCmd
		set nextCmd = call getNextCmd()
		showMsg(call nextCmd.toString());
		raiseEvent cmd(call it.unibo.utility.MessageBuilder.buildMsg(call nextCmd.getName(), %robotName));
		raiseEvent setTimer(call it.unibo.utility.MessageBuilder.buildMsg(call nextCmd.getTimeout(), %robotName))
		if(%isLast) { goToState endOfCommands }
		goToState waitTimeElapsed
	endstate
	
	state endOfCommands
		showMsg("Reached end of commands")
		transitToEnd
	endstate
	
	state waitTimeElapsed
		onEvents timeElapsed or transitRequest goToState check
	endstate			
	
	state check
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		
		if(%eventsForMe==false){
			goToState waitTimeElapsed
		}
		if(call curevId.equals("timeElapsed")) {
			showMsg("TimeElapsed");
			goToState sendCmd
		}
		goToState waitTransitApproval
	endstate
	
	state waitTransitApproval
		showMsg("waitingForApproval")
		onEvent transitApproval goToState checkTransitApproval
	endstate
	
	state checkTransitApproval
		acquireEvent curEventItem
	set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		if(%eventsForMe){
			showMsg("Transit Approved!, raising cmd("+call nextCmd.getName()+")");
			raiseEvent cmd(call it.unibo.utility.MessageBuilder.buildMsg(call nextCmd.getName(), %robotName));
			goToState waitTimeElapsed
		}
		goToState waitTransitApproval
	endstate
}

BehaviorOf mytimer{
	var it.unibo.event.interfaces.IEventItem curEventItem = null
	var String curevId = ""
	var String robotName = ""
	var String remainingTime = ""
	var boolean eventsForMe = false
	
	action void createStoppableTimer(String time)
	action void killTimer()
	
	state init initial
		showMsg("Timer started")
		set robotName = call it.unibo.utility.Configurator.getRobotName()
		goToState waitingForSet
	endstate 
	
	state waitingForSet
		showMsg("waiting for set")
		onEvents setTimer or transitRequest goToState checkStates
	endstate
	
	state checkStates
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		if(%eventsForMe){
			if(call curevId.equals("setTimer")){
				goToState createStoppableTimer
			};
			goToState stopTimer
		}
		goToState waitingForSet
	endstate
	
	state createStoppableTimer
		showMsg("Creating stoppable timer ");
		call createStoppableTimer(call curEventItem.getMsg()) 
		goToState waitingForSet
	endstate
	
	state stopTimer
		call killTimer()
		onEvent transitApproval goToState resumeExecution
	endstate
	
	state resumeExecution
	 showMsg("ricevuto transitApproval timer ")
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		if(%eventsForMe){
			showMsg("re-creating timer "+%remainingTime);
			call createStoppableTimer(%remainingTime);
			goToState waitingForSet
		}
		onEvent transitApproval goToState resumeExecution
	endstate
}

BehaviorOf mutualExclusionCommander{
	var it.unibo.event.interfaces.IEventItem curEventItem = null
	var String curevId = ""
	var boolean eventsForMe = false
	
	var String robotName = ""
	
	state init initial
		showMsg("mutualExclusionCommander ready!")
		set robotName = call it.unibo.utility.Configurator.getRobotName()
		goToState waitForLine
	endstate
	
	state waitForLine
		onEvent lineDetected goToState lineDetectedState
	endstate
	
	state lineDetectedState
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		if(%eventsForMe){
			showMsg("Line detected..");
			raiseEvent cmd(call it.unibo.utility.MessageBuilder.buildMsg("STOP", %robotName));
			raiseEvent transitRequest(%robotName);
			goToState waitApproval	
		}
		goToState waitForLine
	endstate
	
	state waitApproval
		onEvent transitApproval goToState checkApproval
	endstate	
	
	state checkApproval
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		showMsg("checkTransitApproval mutual exclusion commander "+%eventsForMe)
		if(%eventsForMe){
			goToState waitEndTransit
		}
		goToState waitApproval
	endstate
	
	state waitEndTransit
		onEvent lineDetected goToState endLineDetected
	endstate
	
	state endLineDetected
		acquireEvent curEventItem
		set curevId     = call curEventItem.getEventId() 
		set eventsForMe = call it.unibo.utility.EventsForMeChecker.isEventForMe(%curevId,call curEventItem.getMsg(),%robotName)
		if(%eventsForMe) {
			showMsg("End of transit");
			raiseEvent transitComplete(%robotName);
			goToState waitForLine
		}
		goToState waitEndTransit	
	endstate			
}

BehaviorOf semaphoreTask{
	var java.util.ArrayList<String> robotQueue = null
	var it.unibo.event.interfaces.IEventItem curEventItem = null
	var String curevId = ""
	var boolean freeZone = true
	var String robotToApprove = ""
	action void initialize()
	action String dequeue()
	
	state init initial
		showMsg("Semaphore init")
		call initialize()
		goToState waitRequest
	endstate
	
	state waitRequest
		onEvents transitRequest or transitComplete goToState disambiguate
	endstate
	
	state disambiguate
		acquireEvent curEventItem
		set curevId = call curEventItem.getEventId()
		if(call curevId.equals("transitRequest")) {
			goToState enqueue
		}
		goToState zoneFree
	endstate
	
	state zoneFree
		showMsg("robotContext leaved the zone");
		set freeZone = true
		if(call robotQueue.isEmpty()) {
			goToState waitRequest
		}
		goToState approveTransit
	endstate
	
	state enqueue
		showMsg("Request received from "+call curEventItem.getMsg());
		call robotQueue.add(call curEventItem.getMsg())
		goToState approveTransit
	endstate
	
	state approveTransit
		if(%freeZone) {
			set robotToApprove = call dequeue();
			raiseEvent transitApproval(%robotToApprove);
			showMsg("Authorization to "+%robotToApprove);
			set freeZone = false
		}
		goToState waitRequest
	endstate
}

BehaviorOf prenotationSimulator{
	action void sleep(int millis)
	state init initial
		showMsg("Simulator on")
		goToState emitStart
	endstate
	
	state emitStart
		showMsg("Emit start")
		raiseEvent start("Start")
		goToState emitPrenotation
	endstate
	
	state emitPrenotation
		call sleep(5000)
		showMsg("Emit Request")
		raiseEvent transitRequest("Simulator")
		goToState emitTransitComplete
	endstate
	
	state emitTransitComplete
		showMsg("Emit transitcomplete")
		raiseEvent transitComplete("Simulator")
		transitToEnd
	endstate
	
}
