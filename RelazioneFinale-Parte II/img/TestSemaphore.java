package it.unibo.iot.robot.tests;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestRobot
{
	
	@Test
	public void testTransit()
	{
		
		ISemaphore sem = new Semaphore();
		sem.transit("Request");
		assertTrue(sem.getQueue.size() == 0);
		assertFalse(sem.isFree());
		sem.transit("Request2");
		assertTrue(sem.getQueue.size()==1);

	}

	@Test
	public void testTransitComplete()
	{
		ISemaphore sem = new Semaphore();
		assertTrue(sem.isFree());
		sem.transit("Request");
		assertFalse(sem.isFree());	
		sem.transitcomplete("Request");
		assertTrue(sem.isFree());	
	}
}
