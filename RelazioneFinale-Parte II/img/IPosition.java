package it.unibo.utility;

public interface IPosition {
	int getX();
	int getY();
	boolean isOnX();
}
