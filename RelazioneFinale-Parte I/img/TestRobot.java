package it.unibo.iot.robot.tests;

import static org.junit.Assert.*;
import it.unibo.command.utility.CommandFactory;
import it.unibo.command.utility.CommandType;

import org.junit.Test;

public class TestRobot
{
	private MyIRobot robot;
	private CommandFactory factory;
	private ISemaphore semaphore;
	
	@Test
	public void testForwardBackward()
	{
		try
		{
		IRobotPosition initial = robot.getPosition();
		robot.execute(CommandFactory.getInstance().getCommand(CommandType.FORWARD));
		Thread.sleep(1000);
		robot.execute(CommandFactory.getInstance().getCommand(CommandType.BACKWARD));
		Thread.sleep(1000);
		robot.execute(CommandFactory.getInstance().getCommand(CommandType.STOP));
		IRobotPosition last = robot.getPosition();
		
		assertTrue(initial.equals(last));
		
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public void testStop()
	{
		try
		{
		IRobotPosition initial = robot.getPosition();
		robot.execute(CommandFactory.getInstance().getCommand(CommandType.STOP));
		Thread.sleep(1000);
		IRobotPosition last = robot.getPosition();
		
		assertTrue(initial.equals(last));
		
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public void testTransitRequest(){
		robot.setSemaphore(semaphore);
		assertTrue(semaphore.isFree());
		robot.transitRequest();
		assertFalse(semaphore.isFree());
	}

}
