package it.unibo.iot.models.robotCommands;


public interface IRobotCommand {

	public IRobotSpeed getSpeed();

	public String getStringRep();

}
