package it.unibo.iot.robot;

public interface IRobotPosition
{
	public int getX();
	public int getY();
}
