package it.unibo.iot.semaphore.testplan;

import it.unibo.iot.semaphore.interfaces.IController;
import it.unibo.iot.semaphore.interfaces.ISemaphore;
import it.unibo.iot.semaphore.interfaces.ISemaphoreState;
import it.unibo.iot.semaphore.interfaces.IState;

import org.junit.Before;
import org.junit.Test;

public class ControllerTestPlan {

	private IController controller;
	
	@Before
	public void init() {
		ISemaphore pedestrianSemaphore = null;
		ISemaphore vehicleSemaphore = null;
		controller.setPedestrianSemaphore(pedestrianSemaphore);
		controller.setVehicleSemaphore(vehicleSemaphore);
	}
	
	@Test
	public void testIController() {
		assert(controller.getPedestrianSemaphore().getState() == ISemaphoreState.RED);
		assert(controller.getVehicleSemaphore().getState() == ISemaphoreState.GREEN);
		
		controller.changeState(IState.VEHICLEYELLOW);
		
		assert(controller.getPedestrianSemaphore().getState() == ISemaphoreState.RED);
		assert(controller.getVehicleSemaphore().getState() == ISemaphoreState.YELLOW);
		
		controller.changeState(IState.VEHICLERED_PEDESTRIANGREEN);
		
		assert(controller.getPedestrianSemaphore().getState() == ISemaphoreState.GREEN);
		assert(controller.getVehicleSemaphore().getState() == ISemaphoreState.RED);
		
		controller.changeState(IState.PEDESTRIANYELLOW);
		
		assert(controller.getPedestrianSemaphore().getState() == ISemaphoreState.RED);
		assert(controller.getVehicleSemaphore().getState() == ISemaphoreState.YELLOW);
		
		controller.changeState(IState.VEHICLEGREEN_PEDESTRIANRED);
		
		assert(controller.getPedestrianSemaphore().getState() == ISemaphoreState.RED);
		assert(controller.getVehicleSemaphore().getState() == ISemaphoreState.GREEN);
		
	}

}
