package it.unibo.iot.semaphore.interfaces;

public interface IButton {
	
	void addListener(IController controller);
	void press();

}
